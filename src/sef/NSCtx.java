package sef;

import java.io.StringReader;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.zip.ZipInputStream;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.xpath.jaxp.XPathFactoryImpl;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class NSCtx implements NamespaceContext {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5105161050193149620L;
	Hashtable<String, Hashtable<String, String>> nss = new Hashtable<String, Hashtable<String, String>>();
	public String doct = "";
	String xns[][] = {
			{ "container", "dEf", "urn:oasis:names:tc:opendocument:xmlns:container" },
			{ "opf", "dEf", "http://www.idpf.org/2007/opf" },
			{ "opf", "dc", "http://purl.org/dc/elements/1.1/" },
			{ "ncx", "dEf", "http://www.daisy.org/z3986/2005/ncx/" } };

	XPathFactory xpf;
	XPath xpath;
	public XPathExpression autorExp, tituloExp, calibreAutorExp, plistExp;

	public NSCtx() throws XPathExpressionException {
		for (int i = 0; i < xns.length; i++) {
			Hashtable<String, String> au = nss.get(xns[i][0]);
			if (au == null) {
				au = new Hashtable<String, String>();
				nss.put(xns[i][0], au);
			}
			au.put(xns[i][1], xns[i][2]);
		}
		xpf = XPathFactoryImpl.newInstance();
		xpath = xpf.newXPath();
		xpath.setNamespaceContext(this);
		doct = "opf";
		autorExp = xpath.compile("//dc:creator/text()");
		tituloExp = xpath.compile("//dc:title/text()");
		calibreAutorExp = xpath.compile("//dc:creator/@file-as");
		doct = "plist";
		plistExp = xpath.compile("/plist/dict/*");
	}

	/**
	 * Cargamos los namespaces para devolverlos correctamente despues.
	 * 
	 * @param content
	 */
	public void getNSFromXMLDoc(String doct, String content) {
		int pos = 0;
		this.doct = doct;
		while ((pos = content.indexOf("xmlns:", pos)) >= 0) {
			String xmlnsn = "";
			pos += "xmlns:".length();
			while (content.charAt(pos) != '=')
				xmlnsn += content.charAt(pos++);
			pos += 2; // Saltamos = y "
			String url = "";
			while (content.charAt(pos) != '\"')
				url += content.charAt(pos++);
			Hashtable<String, String> au = nss.get(doct);
			if (au == null) {
				au = new Hashtable<String, String>();
				nss.put(doct, au);
			}
			au.put(xmlnsn, url);
		}
	}

	@Override
	public String getNamespaceURI(String prefix) {
		if (prefix == null)
			throw new NullPointerException("Null prefix");
		else if ("xml".equals(prefix))
			return XMLConstants.XML_NS_URI;
		for (int i = 0; i < nss.size(); i++) {
			Hashtable<String, String> dct = nss.get(doct);
			if (dct != null) {
				String ret = dct.get(prefix);
				if (ret != null)
					return ret;
			}
		}
		return XMLConstants.NULL_NS_URI;
	}

	// This method isn't necessary for XPath processing.
	public String getPrefix(String uri) {
		throw new UnsupportedOperationException();
	}

	// This method isn't necessary for XPath processing either.
	@SuppressWarnings("rawtypes")
	public Iterator getPrefixes(String uri) {
		throw new UnsupportedOperationException();
	}

	public NodeList getNodeSet(String doct, String doc, XPathExpression xpe)
			throws XPathExpressionException {
		getNSFromXMLDoc(doct, doc);
		InputSource is = new InputSource(new StringReader(doc.replace("xmlns=",
				"xmlns:dEf=")));
		return (NodeList) xpe.evaluate(is, XPathConstants.NODESET);
	}

	public NodeList getNodeSet(String doct, String doc, String exp)
			throws XPathExpressionException {
		XPathExpression xpe = xpath.compile(exp);
		return getNodeSet(doct, doc, xpe);
	}

	public String getXPathVal(String doct, String xmlDocStr, XPathExpression xpe)
			throws XPathExpressionException {
		xmlDocStr = xmlDocStr.replace("xmlns=", "xmlns:dEf=");
		getNSFromXMLDoc(doct, xmlDocStr);
		InputSource is = new InputSource(new StringReader(xmlDocStr));
		String ret = null;
		try {
			ret = (String) xpe.evaluate(is, XPathConstants.STRING);
		} catch (XPathExpressionException xpee) {
			System.err.println("XPath Expression:" + xpe.toString());
			System.err.println("Data:\n" + xmlDocStr);
			throw xpee;
		}
		return ret;
	}

	public String getXPathVal(String doct, String xmlDocStr, String expres)
			throws XPathExpressionException {
		return getXPathVal(doct, xmlDocStr, xpath.compile(expres));
	}

	public String getXPathVal(String docXml, String doct, ZipInputStream zis,
			String xp) throws Exception {
		return getXPathVal(doct, docXml, xp);
	}

}
