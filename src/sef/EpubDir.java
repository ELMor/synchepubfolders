package sef;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.xpath.XPathExpressionException;

public class EpubDir extends Thread implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -483098843736795863L;
	private transient NSCtx ctx;
	boolean isMaster;
	File name;
	Hashtable<String, EpubBook> fileHTepub = new Hashtable<String, EpubBook>();
	Hashtable<String, Vector<EpubBook>> autorHTVepub=new Hashtable<String, Vector<EpubBook>>();
	Vector<FileSynchMD> stat=null;
	boolean deserialized=false;
	private static int total = 0;
	private static int count = 0;
	private static int lastShown = -1;

	
	public static EpubDir helper(File n) throws IOException, Exception{
		return helper(n,false);
	}
	
	public static EpubDir helper(File n, boolean master) throws IOException, Exception{
		File bin=new File(n,"synchEPUBDir.bin");
		if(!bin.exists())
			return new EpubDir(n.getCanonicalPath(),master);
		EpubDir ret=null;
		try {
			FileInputStream fis=new FileInputStream(bin);
			ObjectInputStream ois=new ObjectInputStream(fis);
			ret=(EpubDir)ois.readObject();
			ois.close();
			ret.deserialized=true;
		} catch (Exception e) {
			ret=new EpubDir(n.getCanonicalPath(),master);
		}
		return ret;
	}
	
	synchronized private static void registerTotal(int t) {
		total += t;
	}

	synchronized private static void registerAvance() {
		count++;
		int pct = count * 1000 / total;
		if (pct > lastShown) {
			int m = pct / 10;
			int n = pct - m * 10;
			lastShown++;
			System.out.print("\b\b\b\b\b\b" + m + "." + n + "%");
		}
	}
	
	private static void totalAvance(){
		System.out.println("");
	}

	public NSCtx getNSCtx(){
		if(ctx!=null)
			return ctx;
		try {
			ctx=new NSCtx();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return ctx;
	}

	protected EpubDir(String d, boolean master ) throws Exception {
		name = new File(d);
		this.isMaster=master;
		if (!name.exists() || !name.isDirectory())
			throw new RuntimeException(new Error(d
					+ " no es un directorio v�lido."));
	}

	public void saveState(){
		File bin=new File(name,"synchEPUBDir.bin");
		try{
			FileOutputStream fos=new FileOutputStream(bin);
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
		}catch(Exception e){
			e.printStackTrace();
			bin.deleteOnExit();
		}
	}
	
	@Override
	public void run() {
		Vector<FileSynchMD> newStat = listar(name, new Vector<FileSynchMD>());
		System.out.println("Found " + newStat.size() + " books on "+ (isMaster ? "Master " : "") + "Directory " + name);
		Vector<String> err;
		if(deserialized){
			Collections.sort(stat);
			Vector<FileSynchMD> rescan=new Vector<FileSynchMD>();
			for(int i=0;i<newStat.size();i++){
				FileSynchMD nue=newStat.get(i);
				int ndx=Collections.binarySearch(stat, nue);
				if(ndx<0){
					rescan.add(nue);
				}else{
					FileSynchMD vie=stat.get(ndx);
					if(vie.modifiedFile(nue))
						rescan.add(nue);
				}	
			}
			err = load(rescan);
		}else{
			//Cargarlos todo los listados
			err = load(newStat);
		}
		if (err.size() > 0) {
			System.out.println("Please correct errors for:");
			for (int i = 0; i < err.size(); i++)
				System.out.println(err.get(i));
			throw new RuntimeException();
		}
		stat=newStat;
		saveState();
	}

	private Vector<FileSynchMD> listar(File dir, Vector<FileSynchMD> fileNames) {
		File[] lista = dir.listFiles();
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].getName().toLowerCase().endsWith(".epub")){
				try {
					fileNames.add(new FileSynchMD(lista[i].getCanonicalPath()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		for (int i = 0; i < lista.length; i++) {
			if (lista[i].isDirectory())
				listar(lista[i],fileNames);
		}
		return fileNames;
	}

	private Vector<String> load(Vector<FileSynchMD> stat) {
		Vector<String> errores = new Vector<String>();
		registerTotal(stat.size());
		for (Enumeration<FileSynchMD> keys = stat.elements(); keys
				.hasMoreElements();) {
			FileSynchMD key = keys.nextElement();
			try {
				EpubBook epb = new EpubBook(this, new File(key.fileName));
				fileHTepub.put(key.fileName, epb);
				Vector<EpubBook> v=autorHTVepub.get(epb.autor);
				if(v==null){
					v=new Vector<EpubBook>();
					autorHTVepub.put(epb.autor, v);
				}
				v.add(epb);
			} catch (Exception e) {
				System.out.println(key + ":\n");
				e.printStackTrace();
				errores.add(key.fileName);
			}
			registerAvance();
		}
		stat = null;
		totalAvance();
		return errores;
	}
	
	public void master_Check() throws IOException {
		if(!isMaster)
			throw new RuntimeException("Dir "+name+" is not Master");
		// Suponemos que los libros est�n en un directorio que contiene el
		// nombre del Autor.
		// Los nombres del epub son APELLIDOS, NOMBRE-TITULO.EPUB
		for(int i=0;i<stat.size();i++){
			FileSynchMD current=stat.get(i);
			EpubBook epf = fileHTepub.get(current.fileName);
			if (!epf.autor.equals(epf.parent)) {
				try {
					// Hay que cambiar dc:creator en el opf...
					System.out.println("Change autor " + epf.titulo + ":"+ epf.autor + "->" + epf.parent);
					epf.changeOnOPF(epf.parent, null);
					epf.autor=epf.parent;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			String fn = epf.file.getName();
			String mustBe = epf.parent + "-" + epf.titulo + ".epub";
			if (!fn.equals(mustBe)) {
				System.out.println("Renaming file to " + mustBe);
				File renombrado=new File(epf.file.getParent(), mustBe);
				epf.file.renameTo(renombrado);
				epf.file=renombrado;
				fileHTepub.remove(current.fileName);
				current.fileName=renombrado.getCanonicalPath();
				fileHTepub.put(current.fileName,epf);
			}
		}
		saveState();
	}

	public void master_synchWith(EpubDir ed) {
		if(!isMaster)
			throw new RuntimeException("Dir "+name+" is not Master");
		
		Hashtable<String, Vector<EpubBook>> master=autorHTVepub;
		Hashtable<String, Vector<EpubBook>> slave=ed.autorHTVepub;
		
		Vector<EpubBook> toAdd=new Vector<EpubBook>();
		Vector<EpubBook> toMod=new Vector<EpubBook>();
		
		for(Enumeration<String> enu=slave.keys();enu.hasMoreElements();){
			String slaveAutor=enu.nextElement();
			Vector<EpubBook> masterBooks=master.get(slaveAutor);
			Vector<EpubBook> slaveBooks=slave.get(slaveAutor);
			if(masterBooks==null){
				toAdd.addAll(slave.get(slaveAutor));
				continue;
			}else{
				for(int i=0;i<slaveBooks.size();i++){
					EpubBook slaveBook=slaveBooks.get(i);
					EpubBook slaveFoundOnMaster=null;
					for(int j=0;j<masterBooks.size();j++){
						EpubBook masterBook=masterBooks.get(j);
						if(slaveBook.esElMismo(masterBook)){
							slaveFoundOnMaster=masterBook;
							break;
						}
					}
					if(slaveFoundOnMaster!=null){
						//Primero vemos el t�tulo del iTunes
						if(!slaveBook.iTunesTitulo.equals(slaveFoundOnMaster.titulo)){
							slaveFoundOnMaster.modOp+="i";
						}
						if(slaveBook.portada>0 && slaveBook.portada!=slaveFoundOnMaster.portada){
							slaveFoundOnMaster.modOp+="p";
						}
						if(slaveFoundOnMaster.modOp.length()>0){
							slaveFoundOnMaster.modBook=slaveBook;
							toMod.add(slaveFoundOnMaster);
						}
					}else{
						toAdd.add(slaveBook);
					}
				}
			}
		}
		
		for(int i=0;i<toAdd.size();i++){
			EpubBook libro=toAdd.get(i);
			System.out.println("Adding "+libro.file);
			master_addBook(libro);
		}
		for(int i=0;i<toMod.size();i++){
			EpubBook book=toMod.get(i);
			System.out.println("Modifying "+book.modOp+" "+book.file+" with "+book.modBook.file);
			master_modBook(book);
		}
	}

	public void master_modBook(EpubBook libro){
		if(libro.modOp.contains("i")){
			try {
				libro.autor=libro.modBook.iTunesAutor;
				libro.titulo=libro.modBook.iTunesTitulo;
				libro.changeOnOPF(libro.autor, libro.titulo);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(libro.modOp.contains("p")){
			try {
				EpubBook portadaFromBook=libro.modBook;
				InputStream is=portadaFromBook.getInputStream("iTunesArtwork");
				if(is!=null){
					libro.setInpuStream("iTunesArtwork", is);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void master_addBook(EpubBook libro){
		String au=libro.autor.trim();
		String le=au.substring(0, 1);
		File origen=libro.file;
		File destino=new File(name,le+File.separator+au+File.separator+origen.getName());
		destino.getParentFile().mkdirs();
		try {
			FileInputStream fis=new FileInputStream(origen);
			FileOutputStream fos=new FileOutputStream(destino);
			EpubBook.writeTo(fis, fos);
			fis.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
