package sef;

import java.io.File;
import java.io.Serializable;

public class FileSynchMD implements Serializable,Comparable<FileSynchMD> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2462474015600211914L;
	public String fileName=null;
	public long modD=0;
	public long size=-1;
	
	public FileSynchMD(String name){
		fileName=name;
		File f=new File(fileName);
		size=f.length();
		modD=f.lastModified();
	}
	
	public boolean sameFile(FileSynchMD other){
		return fileName.equals(other.fileName);
	}
	
	public boolean modifiedFile(FileSynchMD other){
		return sameFile(other) && (size!=other.size || modD!=other.modD);
	}

	@Override
	public int compareTo(FileSynchMD o) {
		return fileName.compareTo(o.fileName);
	}
	
}
